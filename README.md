# Invidition - YouTube to Invidious Redirection

## Introduction
Invidition is an extension allowing you to redirect YouTube and Twitter requests to their counterparts before loading them. Invidition has the purpose to not let any YouTube or Twitter requests pass.

If you like this extension, you can make a donation with the libre currency Ğ1. I don't plan to open donations for any other currency for now.
[Learn more about Ğ1](https://duniter.org/en/introduction/).

Here is my wallet public ID: `C4D3ykicaK5YDs9yTGEURnihCRfVxa36BAhRe2MEG8yq`
You an also directly [click here](https://g1.duniter.fr/#/app/wot/C4D3ykicaK5YDs9yTGEURnihCRfVxa36BAhRe2MEG8yq/).

## Downloads
### Firefox
You can download it for Firefox here: https://addons.mozilla.org/firefox/addon/invidition

### Others
I have no Google account. I don't plan to host it on Chrome Play Store. Sources are open and the license is permissive (Public Domain Dedication) so feel free to host it on Chrome Store if you want.

## Build from Sources
To build this extension from sources, you first need to install [nodejs](https://nodejs.org/).

* `git clone https://gitlab.com/Booteille/invidition.git`
* `cd invidition/`
* `npm install`
* `npm run build`

Then. You can either use `make package` to generate `extension.zip` and `sources.zip` and load `extension.zip` in your browser, either go in the `extension/` folder and run `web-ext run`. 


## Compatibility with uMatrix and NoScript
Invidition is compatible with any other extension, but you will need to configure extensions using WebRequest API, like uMatrix and NoScript.
The reason is because these addons tend to block requests before Invidition. In such cases, Invidition never knows it has a request to redirect.
I'll investigate if there is a way to give priority to Invidition over uMatrix and NoScript.

Rulesets and recipes provided are set for invidio.us. If you want to use Invidition for another instance, please, download and update files accordingly.

### uMatrix Ruleset and Recipe
#### Rules
You can import [this ruleset](https://gitlab.com/Booteille/invidition/blob/master/umatrix/rules.txt) if you want to allow Invidition to automatically work without any action from your part.

To import a ruleset, you have to open uMatrix settings then:
* Click on *My Rules* tab.
* Click on *Import from a file* and select the rules you downloaded on your computer.
* Click on *Apply*.

#### Recipe
You can use [the recipe](https://gitlab.com/Booteille/invidition/raw/master/umatrix/recipe.txt) if you want to be able to allow Invidition to work with most popular instances in two clicks.

To import the recipe, open uMatrix settings then:
* Click on *Resources*.
* Under *Rulessets Recipes", click on *Import*.
* In the text area, grab this URL: 
> `https://gitlab.com/Booteille/invidition/raw/master/umatrix/recipe.txt` 
* Click on *Apply*.

### NoScript
You just need to allow requests from your selected instance.

### Why does I need to allow scripts and frames?

`youtube.com` and `s.ytimg.com` scripts are mandatory because of the YouTube iFrame API.
Some websites (like joinmastodon.org) use this API to generate the iFrame.
So, the browser needs first to load a script called `iframe_api` on `youtube.com` and then load a script called `www-widgetapi.js` on `s.ytimg.com`.
To bypass this need, those two files have been integrated in the extension, so you never have to call youtube to generate the iFrame.

The reason why you need to allow frames is the same explained above: Invidition can't read the iFrame URL if you block it.
