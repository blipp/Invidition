import * as _ from "lodash";
import * as InvidiousManager from "./lib/API/invidious";
import * as NitterManager from "./lib/API/nitter";
import {domainToUrl, info} from "./lib/helpers";
import * as SettingsManager from "./lib/settings";

/**
 * Rewrite an URL to its alternative
 *
 * @param link HTMLAnchorElement
 * @returns {Promise<any>}
 */
export const rewriteLink = async (link: HTMLAnchorElement) => {
    const settings = await SettingsManager.load();
    const iDomains = settings.invidious.appSettings.domains.toRedirect;
    const nDomains = settings.nitter.appSettings.domains.toRedirect;
    const iMustRewrite = settings.invidious.appSettings.isEnabled && settings.invidious.appSettings.rewriteLinks;
    const nMustRewrite = settings.nitter.appSettings.isEnabled && settings.nitter.appSettings.rewriteLinks;
    const url = new URL(link.href);
    let newUrl = null;

    if (iMustRewrite && _.includes(iDomains, url.hostname)) {
        newUrl = (await InvidiousManager.generateUrl(url));
    } else if (nMustRewrite && _.includes(nDomains, url.hostname)) {
        newUrl = (await NitterManager.generateUrl(url));
    }

    if (newUrl) {
        link.href = newUrl.href;

        if (_.includes(link.innerHTML, url.hostname)) {
            link.innerHTML = link.innerHTML.replace(url.hostname, newUrl.hostname);
        }

        info(`${url.href} rewritten.`);
    }

    return link;
};

const updateDOM = async () => {
    const anchors = document.getElementsByTagName("A");

    let i = 0;
    for (i; i < anchors.length; ++i) {
        rewriteLink(anchors[i] as HTMLAnchorElement);
    }
};

const run = async () => {
    const settings = await SettingsManager.load();
    if (settings.isEnabled) {
        updateDOM();
        const throttledUpdateDOM = _.throttle(updateDOM, 500);
        const observer = new MutationObserver((mutationsList) => {
            for (const mutation of mutationsList) {
                throttledUpdateDOM();
            }
        });

        observer.observe(document.body, {
            childList: true,
            subtree: true,
        });
    }
};

try {
    run();
} catch (e) {
    console.error(e.message);
}
