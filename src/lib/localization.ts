// @ts-ignore
import ISO6391 from "iso-639-1";
import * as _ from "lodash";

export const normalizeLanguageCodeToBrowser = (code: string) => {
    try {
        return _.replace(code, "_", "-");
    } catch (e) {
        console.error(e.message);
    }
};

export const trimLocaleSubtag = (code: string) => {
    try {
        return ISO6391.getNativeName(code.substr(0, 2));
    } catch (e) {
        console.error(e.message);
    }
};
