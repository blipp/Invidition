import * as _ from "lodash";
// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import * as InvidiousManager from "./API/invidious";
import * as CacheManager from "./cache";
import TimeOutError from "./Error/TimeOutError";
import * as SettingsManager from "./settings";

export enum LOG_TYPE {
    ERROR,
    INFO,
}

export enum PING_ERROR {
    ERROR,
    TIMEOUT,
    TYPE,
}

export enum FILTER {
    CSP,
    REDIRECT
}

export enum SERVICE {
    INVIDIOUS,
    NITTER,
    CSP,
}

/**
 * Generate redirection filters from domains
 * @param {FILTER} type
 * @param {SERVICE} service
 * @returns {Promise<Array<string> | unknown[]>}
 */
export const filterDomains = async (type: FILTER, service?: SERVICE) => {
    const domains = type === FILTER.CSP ? await domainsToInterceptCSP() : await domainsToRedirect();
    const settings = await SettingsManager.load();

    let invidious: Array<string> = _.map(domains.invidious,
                                         (d: string) => d !== settings.invidious.appSettings.instance ? `*://${d}/*` : `*://${d}/`,
    );

    if (invidious.indexOf(`*://${settings.invidious.appSettings.instance}/`) !== -1) {
        _.forEach(settings.invidious.appSettings.domains.validPaths, (path) => {
            invidious.push(`*://${settings.invidious.appSettings.instance}${path}*`);
        });
    }

    const nitter: Array<string> = _.map(domains.nitter, (d: string) => `*://${d}/*`);
    const csp: Array<string> = type === FILTER.CSP ? _.map(domains.csp, (d: string) => `*://${d}/*`) : [];

    if (service === SERVICE.INVIDIOUS) {
        return invidious;
    } else if (service === SERVICE.NITTER) {
        return nitter;
    } else if (service === SERVICE.CSP) {
        return csp;
    }

    return FILTER.CSP ? _.concat(invidious, nitter, csp) : _.concat(invidious, nitter);
};

/**
 *
 * @param {FILTER} type
 * @param {SERVICE} service
 * @returns {Promise<any[]>}
 */
export const filterTypes = async (type: FILTER, service?: SERVICE) => {
    // const invidious = [
    //     "image",
    //     "imageset",
    //     "main_frame",
    //     "media",
    //     "object",
    //     "script",
    //     "sub_frame",
    // ];

    // if (service === SERVICE.INVIDIOUS) {
    //     return invidious;
    // }

    // TODO: Add types for other services

    return [
        "image",
        "imageset",
        "main_frame",
        "media",
        "object",
        "script",
        "sub_frame",
    ];
    ;
};

/**
 * Returns array of domains needing to change their CSP policy
 * @returns {Promise<unknown[]>}
 */
export const domainsToInterceptCSP = async () => {
    try {
        const settings = await SettingsManager.load();
        const iDomains = settings.invidious.appSettings.domains;
        const nDomains = settings.nitter.appSettings.domains;

        return {
            invidious: _.concat(iDomains.toRedirect, settings.invidious.appSettings.instance),
            nitter: _.concat(nDomains.toRedirect, settings.nitter.appSettings.instance),
            csp: _.concat(iDomains.toInterceptCSP, nDomains.toInterceptCSP),
        };
    } catch (e) {
        console.error(e);
    }
};

/**
 * Get domains to redirect
 *
 * @returns {Promise<{invidious: any[]; nitter: any[]}>}
 */
export const domainsToRedirect = async () => {
    try {
        const settings = await SettingsManager.load();

        let domains = {
            invidious: [],
            nitter: [],
            csp: [],
        };

        domains.invidious = _.concat(domains.invidious, settings.invidious.appSettings.domains.toRedirect);
        domains.invidious.push(settings.invidious.appSettings.instance);

        domains.nitter = _.concat(domains.nitter, settings.nitter.appSettings.domains.toRedirect);
        domains.nitter.push(settings.nitter.appSettings.instance);

        if (settings.invidious.appSettings.alwaysUsePreferredInstance) {
            const instances = await InvidiousManager.getAvailableInstances();
            _.forEach(instances, (instance) => {
                domains.invidious.push(instance[0]);
            });
        }

        return domains;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Transform the domain name in an URL
 *
 * @param {string} domain
 * @returns {URL}
 */
export const domainToUrl = (domain: string) => {
    if (_.isEmpty(domain)) {
        throw new console.error("Domain can't be empty");
    }

    domain = _.endsWith(domain, ".onion") || _.endsWith(domain, ".i2p") || domain === "localhost"
        ? `http://${domain}` : `https://${domain}`;

    return new URL(domain);
};

/**
 * Helper for error logs
 *
 * @param message
 */
export const error = (message) => {
    log(message, LOG_TYPE.ERROR);
};

/**
 * Try to get cookies from the hostname.
 * Firefox and Icecat does not handle cookies.getAll() in the same manner so here is a workaround.
 *
 * @param {string} hostname
 * @param name
 * @returns {Promise<browser.cookies.Cookie[]>}
 */
export const getCookies = async (hostname: string, name?: string) => {
    try {
        if (browser.cookies !== undefined) {
            let cookies = await browser.cookies.getAll({domain: hostname, name, firstPartyDomain: getDomain(hostname)});

            if (isEmpty(cookies)) {
                cookies = await browser.cookies.getAll({domain: hostname, name, firstPartyDomain: null});
            }

            return cookies;
        }

        return [];
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Extract domain from hostname
 * @param {string} hostname
 * @returns {string}
 */
export const getDomain = (hostname: string) => {
    const domain = hostname.substr(hostname.indexOf(".") + 1);

    if (domain.indexOf(".") === -1) {
        return hostname;
    }

    return domain;
};

/**
 * Check if the browser can write to clipboard
 * @returns {boolean}
 */
export const canWriteToClipboard = () => {
    return navigator.clipboard !== undefined;
};

/**
 * Check if the app or the service is enabled
 */
export const isEnabled = async (service?: string) => {
    try {
        const settings = await SettingsManager.load();

        if (service) {
            return settings[service].appSettings.isEnabled;
        }

        return settings.isEnabled;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Helper for info logs
 * @param message
 */
export const info = (message: any) => {
    log(message, LOG_TYPE.INFO);
};

/**
 * Check if a value is empty
 * @param value
 * @returns {boolean | any}
 */
export const isEmpty = (value: any) => {
    if (Array.isArray(value)) {
        return value.every((i) => _.isEmpty(i));
    } else if (typeof value === "boolean" || typeof value === "number") {
        return false;
    }

    return _.isEmpty(value);
};

/**
 * Check if the browser is Tor
 * @returns {Promise<boolean>}
 */
export const isTor = async () => {
    const cached = await CacheManager.get("isTor");

    if (cached && (!_.includes(["string", "object", "array"], typeof cached.data) || !_.isEmpty(
        cached.data)) && await CacheManager.validate(
        cached, 3600 * 24)) {
        return cached.data;
    }

    const t = await ping("http://expyuzz4wqqyqhjn.onion/", 500, false); // Try to ping Onion version of www.torproject.org
    const r = PING_ERROR.TYPE !== t;

    CacheManager.save("isTor", r);
    return r;
};

/**
 * Log message to console
 * @param message
 * @param {LOG_TYPE} type
 */
export const log = (message: any, type?: LOG_TYPE) => {
    const preMessage = "[Invidition] ";

    switch (type) {
        case LOG_TYPE.ERROR:
            // tslint:disable-next-line:no-console
            console.error(preMessage + message);
            break;
        case LOG_TYPE.INFO:
            // tslint:disable-next-line:no-console
            console.info(preMessage + message);
            break;
        default:
            // tslint:disable-next-line:no-console
            console.log(preMessage + message);
    }
};

/**
 * Ping an URL
 * @param {string} url
 * @param {number} timeout
 * @param {boolean} logError
 * @returns {Promise<number | PING_ERROR.TIMEOUT | PING_ERROR.ERROR>}
 */
export const ping = async (url: string, timeout = 3000, logError = true) => {
    const TIME_START = new Date().getTime();
    const controller = new AbortController();
    const signal = controller.signal;

    const headers = new Headers();
    headers.append("pragma", "no-cache");
    headers.append("cache-control", "no-cache");

    const fetchPromise = fetch(url, {headers, signal});
    setTimeout(() => controller.abort(), timeout);

    return fetchPromise.then((r) => {
        if (!r.ok) {
            throw new console.error(`HTTP ${r.status} error while trying to fetch "${r.url}"`);
        }

        return new Date().getTime() - TIME_START;
    }).catch((e) => {
        if (e.name === "AbortError") {
            if (logError) {
                console.error(new TimeOutError(`The operation has timed out while trying to fetch "${url}"`));
            }

            return PING_ERROR.TIMEOUT;
        } else {
            if (logError) {
                console.error(e);
            }

            return e.name === "TypeError" ? PING_ERROR.TYPE : PING_ERROR.ERROR;
        }
    });
};
