import * as _ from "lodash";
import {config} from "../../config/config";
import * as CacheManager from "../cache";
import {domainToUrl, getCookies, info, isEmpty, isTor, ping, PING_ERROR} from "../helpers";
import * as SettingsManager from "../settings";

/**
 * Apply user settings to an URL
 * @param {URL} url
 * @returns {Promise<URL>}
 */
export const applySearchParams = async (url: URL) => {
    try {
        const settings = await SettingsManager.load();

        if (false === settings.invidious.appSettings.useParameters) {
            if (url.pathname === "/watch" && settings.invidious.instanceSettings.listen) {
                    url.searchParams.set("listen", "1");
            }
            return url;
        }

        const cookies = await getCookies(url.hostname, "PREFS");

        const interfaceSettings = [
            "hl",
            "dark_mode",
            "thin_mode",
        ];

        let cookieSettings = null;
        if (!isEmpty(cookies)) {
            cookieSettings = JSON.parse(decodeURIComponent(cookies[0].value));
        }

        _.forEach(settings.invidious.instanceSettings, (v, k) => {
            let ck = k; // Cookie Key
            let cv = v; // value for compare with cookie

            switch (k) {
                case "autoplay":
                    if (!v && url.searchParams.get("controls") === "0") {
                        url.searchParams.set("controls", "1");
                    }

                    v = v ? 1 : 0;
                    break;
                case "continue":
                case "listen":
                case "local":
                case "nojs":
                    // Workaround for old versions not handling true as parameter
                    v = v ? 1 : 0;
                    break;
                case "subtitles":
                    cv = v.split(",");
                    ck = "captions";
                    v = cv;
                    break;
                case "dark_mode":
                    cv = v ? "dark" : "light";
                    break;
                case "hl":
                    ck = "locale";
                    break;
                case "loop":
                    ck = "video_loop";
                    v = v ? 1 : 0;
                    break;
            }

            // If there are no cookies or setting if different from the cookie
            if (!isEmpty(v) && (cookieSettings === null || (settings.invidious.appSettings.overrideCookies && cv !== cookieSettings[ck]))) {
                // if the path is good
                if (_.startsWith(url.pathname, "/embed") || _.startsWith(url.pathname, "/watch")
                    || interfaceSettings.indexOf(k) !== -1) {
                    if (v !== "dash" || url.searchParams.get("quality") !== "medium") { // avoid issue with dash and geoblocked videos.
                        url.searchParams.set(k, v);
                    }
                }
            } else {
                url.searchParams.delete(k);
            }
        });
    } catch (e) {
        console.error(e.message);
    }

    return url;
};

/**
 * Return a cleaned version of the Invidious URL.
 *
 * @param {URL} url
 * @returns {URL}
 */
export const cleanedUrl = (url: URL) => {
    try {
        const newUrl = new URL(url.origin);
        newUrl.pathname = url.pathname;

        if (url.pathname === "/watch") {
            newUrl.searchParams.set("v", url.searchParams.get("v"));
        }

        return newUrl;

    } catch (e) {
        console.error(e.message);
    }
};

export const fetchAPI = async (action: string, params?: any) => {
    try {
        const settings = await SettingsManager.load();
        const paramString = params !== undefined ? (typeof params === "string"
            ? params
            : Object.keys(params).map((key) => {
                return `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`;
            }).join("&")) : null;

        const url = `${domainToUrl(
            settings.invidious.appSettings.instance)}${settings.invidious.appSettings.api.endpoint}/${action}${paramString ? `/${paramString}` : ""}`;
        return fetch(url)
            .then((res) => res.json())
            .catch((e) => console.error(
                `An error occurred while trying to fetch Invidious API: ${e.message}`,
            ));
    } catch (e) {
        console.error(e.message);
    }
};

const fetchInstancesAPI = async (sortBy?: string) => {
    try {
        const url = `${config.invidious.appSettings.api.instancesList}${sortBy ? `?sort_by=${sortBy}` : ""}`;

        const cached = await CacheManager.get("instancesList");

        if (cached && !_.isEmpty(cached.data) && await CacheManager.validate(cached, 300)) {
            return cached.data;
        }

        return fetch(url)
            .then(async (res) => {
                const json = await res.json();
                await CacheManager.save("instancesList", json);
                return json;
            })
            .catch(async (e) => {
                console.error(e.message);

                if (cached && !_.isEmpty(cached.data)) {
                    return cached.data;
                } else {
                    return [];
                }
            });
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Generate the Invidious Url
 *
 * @param {URL} oldUrl      The URL to apply settings
 * @param {URL} newUrl      The URL of the instance
 * @returns {Promise<string | URL>}
 */
export const generateUrl = async (oldUrl: URL) => {
    try {
        const ytFiles = [
            "iframe_api",
            "www-widgetapi",
        ];

        // Use local files for Iframe API
        for (const f of ytFiles) {
            if (_.includes(oldUrl.pathname, f)) {
                const path = browser.runtime.getURL(`/assets/js/youtube/${f}.js`);

                return new URL(path);
            }
        }

        const settings = await SettingsManager.load();
        let newUrl = domainToUrl(settings.invidious.appSettings.instance);

        newUrl.pathname = oldUrl.pathname;
        newUrl.search = oldUrl.search;
        newUrl = await applySearchParams(newUrl);

        return newUrl;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Get available instances for the browser
 * @returns {Promise<any[]>}
 */
export const getAvailableInstances = async (sortBy?: string, toExclude = []) => {
    try {
        if (!(await isTor())) {
            toExclude.push(["type", "onion"]);
            sortBy = "location";
        } else {
            sortBy = "type-reverse";
        }
        toExclude.push(["type", "i2p"]);

        return (await getInstances(sortBy, toExclude));
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Get the fastest instance available
 * @param {string} sortBy
 * @param {any[]} toExclude
 * @returns {Promise<any>}
 */
export const getFastestInstance = async (sortBy?: string, toExclude = []) => {
    try {
        if (await isTor()) {
            toExclude.push(["type", "https"]);
        }

        const instances = await getInstancesPing(sortBy, toExclude);

        info(`The fastest instance is ${instances[0][0][0]}`);
        return instances[0][0];
    } catch (e) {
        console.error(e.message);
    }
};

/**
 *
 * @param {string} sortBy
 * @param {Array<[string, string]>} exclude
 * @returns {Promise<any[]>}
 */
export const getInstances = async (sortBy?: string, exclude?: Array<[string, string]>) => {
    try {
        const instances: any[] = await fetchInstancesAPI(sortBy);

        if (exclude) {
            _.forEach(exclude, (rule) => {
                _.remove(instances, (instance): any => {
                    if (instance[1][rule[0]] === rule[1]) {
                        return true;
                    }
                });
            });
        }

        return instances;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Get instances ordered by ping
 * @param {string} sortBy
 * @param {any[]} toExclude
 * @returns {Promise<unknown[]>}
 */
export const getInstancesPing = async (sortBy?: string, toExclude = []) => {
    try {
        const instances = await getAvailableInstances(null, toExclude);
        const sortedInstances = [];

        info("Pinging instances...");
        const settings = await SettingsManager.load();

        if (settings.invidious.appSettings.alwaysUsePreferredInstance) {
            settings.invidious.appSettings.alwaysUsePreferredInstance = false;

            await SettingsManager.save(settings);
        }

        for (const i of instances) {
            const t = await ping(i[1].uri, await isTor() ? 10000 : 1500, false);

            // Exclude instances we couldn't fetch
            if (t !== PING_ERROR.ERROR && t !== PING_ERROR.TIMEOUT) {
                sortedInstances.push([i, t]);
            }
        }

        if (settings.invidious.appSettings.alwaysUsePreferredInstance) {
            settings.invidious.appSettings.alwaysUsePreferredInstance = true;

            await SettingsManager.save(settings);
        }
        info("Pong!");

        return _.sortBy(sortedInstances, 1);
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Returns the version of the software
 * @returns {Promise<any>}
 */
export const getVersion = async () => {
    try {
        return (await fetchAPI("stats")).software.version;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Get a random instance
 * @param {string} sortBy
 * @param {any[]} toExclude
 * @returns {Promise<any>}
 */
export const getRandomInstance = async (sortBy?: string, toExclude = []) => {
    try {
        if (await isTor()) {
            toExclude.push(["type", "https"]);
        }

        const instances = await getAvailableInstances(sortBy, toExclude);

        return instances[_.random(0, instances.length - 1)];
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Check if the provided URL can be cleaned
 * @param {URL} url
 * @returns {boolean}
 */
export const isCleanable = (url: URL) => {
    try {
        return url.pathname === "/watch";
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Check if the instance is not known
 * @param {string} instance
 * @returns {Promise<boolean>}
 */
export const isCustomInstance = async (instance: string) => {
    try {
        const instances = await getAvailableInstances();

        for (const i of instances) {
            if (i[0] === instance) {
                return false;
            }
        }

        return true;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Check if the path is valid
 * @param {URL} url
 * @returns {boolean}
 */
export const isValidPath = (url: URL): boolean => url.pathname === "/" || _.some(config.invidious.appSettings.domains.validPaths,
                                                                                 (o) => _.startsWith(url.pathname, o),
);

/**
 * Check if the url must be redirected
 * @param {{url: string; type: string}} r
 * @returns {Promise<boolean>}
 */
export const mustRedirect = async (r: { url: string; type: string; }): Promise<boolean> => {
    try {
        const settings = (await SettingsManager.load());

        const oldUrl = new URL(r.url);
        let newUrl = new URL(r.url);

        if (!settings.invidious.appSettings.isEnabled || (oldUrl.hostname === settings.invidious.appSettings.instance && !isValidPath(
            oldUrl))) {
            return false;
        }

        newUrl = r.type === "main_frame" ? await generateUrl(newUrl) : newUrl;

        return newUrl.hostname !== settings.invidious.appSettings.instance || oldUrl.href !== newUrl.href;
    } catch (e) {
        console.error(e.message);
    }

    return false;
};

// export const getVideo = async (id: string) => fetchAPI("videos", id);
//
// export const getChannel = async (ucid: string) => fetchAPI("channels", ucid);
