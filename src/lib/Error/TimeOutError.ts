// @ts-ignore
import CustomError from "./CustomError";

// @ts-ignore
export default class TimeOutError extends CustomError {
    constructor(message?) {
        message = message ? message : "The operation has timed out";
        super(message);

        this.name = "TimeOutError";
    }
}
