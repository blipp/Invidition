import * as _ from "lodash";
// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import {config} from "../config/config";
import * as InvidiousManager from "./API/invidious";
import {toggleIcon} from "./browser_action";
import {Event} from "./events";
import {error, isEnabled, isTor} from "./helpers";
import * as InternationalizationManager from "./i18n";
import * as SettingsManager from "./settings";

export const addSpinIcon = (e) => {
    try {
        let el = "";
        if (e instanceof HTMLButtonElement) {
            el = e.innerHTML;
            e.innerHTML = `${el} <img class="spin" height="10px" src="/assets/img/spin.svg" alt="spin">`;
            e.disabled = true;
        } else if (e instanceof HTMLSelectElement) {
            const spin = document.createElement("IMG") as HTMLImageElement;
            spin.src = "/assets/img/spin.svg";
            spin.height = 10;
            spin.id = Date.now().toString(16);
            spin.classList.add("spin");

            e.parentNode.insertBefore(spin, e);

            el = spin.id;
        }

        return el;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * create URL query for captions from DOM
 * @returns {string}
 */
const captionsToQuery = (): string => {
    try {
        let query = "";

        _.forEach(document.getElementsByClassName("defaultCaptions"), (element: HTMLOptionElement) => {
            query += `${element.value || ""},`;
        });

        return query.substr(0, query.length - 1);
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Generate the dropdown of Invidious Instances
 * @returns {Promise<void>}
 */
export const generateInvidiousInstancesDropdown = async () => {
    try {
        const dropdown = document.getElementById("invidious-appSettings-instance") as HTMLFormElement;

        if (dropdown) {
            const spin = addSpinIcon(dropdown);

            const instances: any = await InvidiousManager.getAvailableInstances();
            const settings = await SettingsManager.load();

            _.forEach(instances, (instance) => {
                const groupID = `invidious-instances-label${instance[1].type}`;
                let group = null;

                const option: HTMLOptionElement = document.createElement("OPTION") as HTMLOptionElement;
                option.value = instance[0];
                option.innerText = instance[1].flag ? `${instance[1].flag} ${instance[0]}` : instance[0];

                if (!dropdown.contains(document.getElementById(groupID))) {
                    group = document.createElement("OPTGROUP") as HTMLOptGroupElement;
                    group.label = instance[1].type;
                    group.id = groupID;

                    dropdown.append(group);
                } else {
                    group = document.getElementById(groupID);
                }

                group.append(option);

                if (settings.invidious.appSettings.instance === option.value) {
                    dropdown.value = option.value;
                }

            });

            removeSpinIcon(dropdown, spin);
        }
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Parse DOM to retrieve user settings
 * @return [description]
 */
export const getSettingsFromDOM = async () => {
    try {
        const form = document.getElementById("invidition-form") as HTMLFormElement;
        const availableServices = ["invidious", "nitter"];
        const settings = await SettingsManager.load();

        _.forEach(form, (e) => {
            if (e.id === "") {
                return;
            }

            const splitID = e.id.split("-");
            const service = splitID[0];
            const category = splitID[1];

            if (service === "isEnabled") {
                settings.isEnabled = (e as HTMLInputElement).checked;
                return;
            } else if (availableServices.indexOf(service) === -1) {
                throw new Error(`The service name "${service} on id "${e.id}" is not supported. Check your options.html template!`);
            }

            const optionName = e.id.split("-")[2];

            switch (e.tagName) {
                case "INPUT":
                    if ((e as HTMLInputElement).type === "checkbox") {
                        settings[service][category][optionName] = (e as HTMLInputElement).checked;
                    } else {
                        settings[service][category][optionName] = (e as HTMLInputElement).value;
                    }

                    break;
                case "SELECT":
                    if (e.className === "defaultCaptions") {
                        settings[service][category].subtitles = captionsToQuery();
                    } else {
                        settings[service][category][optionName] = (e as HTMLSelectElement).value;
                    }

                    break;
            }
        });

        return settings;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Initialize Tor rendering for forms
 * @returns {Promise<void>}
 */
export const initTor = async () => {
    try {
        if (await isTor()) {
            const dropdown = document.getElementById("invidious-appSettings-instance");
            const torWarning = document.createElement("A") as HTMLLinkElement;

            torWarning.dataset.i18nTitle = "torWarning";
            torWarning.href = "https://2019.www.torproject.org/docs/onion-services.html.en";
            torWarning.innerHTML = "<img alt='Warning' height='10px' src='/assets/img/exclamation-triangle.svg'/>";
            torWarning.rel = "noreferrer";
            torWarning.rel = "noopener";
            torWarning.setAttribute("target", "_blank");

            dropdown.parentNode.insertBefore(torWarning, dropdown);
        }
    } catch (e) {
        console.error(e.message);
    }
};

export const removeSpinIcon = (e, value) => {
    try {
        if (e instanceof HTMLButtonElement) {
            e.innerHTML = value;
            e.disabled = false;
        } else if (e instanceof HTMLSelectElement) {
            document.getElementById(value).remove();
        }
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Save settings from DOM
 * @return Promise<T>
 */
export const saveForm = async (e?) => {
    try {
        if (e) {
            e.preventDefault();
        }

        if (validateForm()) {
            await saveSettings();
        } else {
            error("Unable to save settings. Form is not valid");
        }
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Save settings from DOM
 * @return Promise<T>
 */
export const saveSettings = async () => {
    try {
        await SettingsManager.save(await getSettingsFromDOM());
    } catch (e) {
        console.error(e.message);
    }
};

export const selectFastestInstance = async (e) => {
    try {
        const buttonValue = addSpinIcon(e.target);
        const instance = await InvidiousManager.getFastestInstance();

        const dropdown = document.getElementById("invidious-appSettings-instance") as HTMLSelectElement;
        dropdown.value = instance ? instance[0] : dropdown.value;
        await saveForm();

        removeSpinIcon(e.target, buttonValue);
    } catch (e) {
        console.error(e.message);
    }
};

export const toggleCustomInstance = async () => {
    try {
        const ID = "invidious-appSettings-instance";
        const element = document.getElementById(ID);

        if (element instanceof HTMLSelectElement) {
            const inputElement = document.createElement("INPUT") as HTMLInputElement;

            inputElement.id = ID;
            inputElement.type = "text";
            inputElement.value = element.value;
            inputElement.pattern = "^(localhost)|((?!-)([a-zA-Z0-9-.]?)+\\.([a-zA-Z]{2,}|i2p))$";
            inputElement.dataset.i18nTitle = "customInstanceHint";
            inputElement.autocomplete = "off";
            inputElement.autocapitalize = "off";
            inputElement.spellcheck = false;
            inputElement.required = true;
            inputElement.classList.add("instance");

            element.parentNode.replaceChild(inputElement, element);
            inputElement.focus();
        } else {
            const selectElement = document.createElement("SELECT") as HTMLSelectElement;
            selectElement.id = ID;
            selectElement.classList.add("instance");

            element.parentNode.replaceChild(selectElement, element);
            await generateInvidiousInstancesDropdown();

            selectElement.value = (element as HTMLInputElement).value;
        }

        await InternationalizationManager.replaceDOM();
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Adapt view in function of the extension state
 */
export const toggleEnable = async (e?) => {
    try {
        const nitterFieldset = (document.getElementById("nitter-fieldset") as HTMLFieldSetElement);
        const invidiousFieldset = (document.getElementById("invidious-fieldset") as HTMLFieldSetElement);

        if (e) {
            if (e.target.id !== "isEnabled") {
                const service = e.target.id.split("-")[0];
                const fieldset = (document.getElementById(`${service}-fieldset`) as HTMLFieldSetElement);
                const wasDisabled = fieldset.disabled;

                fieldset.disabled = !e.target.checked;

                if (wasDisabled) {
                    saveForm();
                }
            } else {
                const nitterEnabled = await isEnabled("nitter");
                const invidiousEnabled = await isEnabled("invidious");

                nitterFieldset.disabled = !nitterEnabled;
                invidiousFieldset.disabled = !invidiousEnabled;
            }
        } else {
            const nitterEnabled = await isEnabled("nitter");
            const invidiousEnabled = await isEnabled("invidious");

            nitterFieldset.disabled = !nitterEnabled;
            invidiousFieldset.disabled = !invidiousEnabled;
        }

        if (!(await isEnabled())) {
            _.forEach(document.getElementsByTagName("FIELDSET"), (fieldset) => {
                (fieldset as HTMLFieldSetElement).disabled = true;
            });
        }

        toggleIcon();
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Update the DOM from settings
 */
export const updateDOM = async () => {
    try {
        const settings = await SettingsManager.load();

        _.forEach(settings, (options, service) => {
            // Only parse invidious and nitter settings
            if (service !== "invidious" && service !== "nitter") {
                if (service === "isEnabled") {
                    const el = document.getElementById("isEnabled") as HTMLInputElement;
                    if (el) {
                        el.checked = options;
                    }
                }
                return;
            }

            _.forEach(options, (category, categoryName) => { // Iterate over categories
                _.forEach(category, (value, name) => { // Iterate over settings
                    if (name !== "subtitles") {
                        const e: HTMLElement = document.getElementById(`${service}-${categoryName}-${name}`) as HTMLElement;

                        if (e !== null) {
                            switch (e.tagName) {
                                case "INPUT":
                                    if ((e as HTMLInputElement).type === "checkbox") {
                                        (e as HTMLInputElement).checked = _.defaultTo(value === true || value === 1,
                                                                                      config[service][categoryName][name],
                                        );
                                    } else {
                                        (e as HTMLInputElement).value = _.defaultTo(value, config[service][categoryName][name]);
                                    }

                                    break;
                                case "SELECT":
                                    (e as HTMLSelectElement).value = _.defaultTo(value, config[service][categoryName][name]);

                                    break;
                                default:
                                    return;
                            }
                        }
                    } else {
                        _.forEach(_.split(value, ","), (v, k) => {
                            const el = document.getElementById(
                                `${service}-${categoryName}-defaultCaptions${k + 1}`) as HTMLSelectElement;

                            if (el) {
                                el.value = v;
                            }

                        });
                    }
                });
            });
        });
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Check if form inputs are valid
 * @returns {boolean}
 */
export const validateForm = () => {
    try {
        const invidiousInstance = document.getElementById("invidious-appSettings-instance") as HTMLInputElement;
        const nitterInstance = document.getElementById("nitter-appSettings-instance") as HTMLInputElement;
        const regex = /^(localhost)|((?!-)([a-zA-Z0-9-.]?)+\.([a-zA-Z]{2,}|i2p))$/;
        let isValid = true;

        if (false === regex.test(invidiousInstance.value)) {
            invidiousInstance.setCustomValidity(invidiousInstance.title);

            isValid = false;
        } else {
            invidiousInstance.setCustomValidity("");
        }

        if (false === regex.test(nitterInstance.value)) {
            nitterInstance.setCustomValidity(nitterInstance.title);

            isValid = false;
        } else {
            nitterInstance.setCustomValidity("");
        }

        return isValid;
    } catch (e) {
        console.error(e.message);
    }
};

try {
    browser.runtime.onMessage.addListener((message) => {
        if (message === Event.SETTINGS_SAVE) {
            updateDOM();
        }
    });
} catch (e) {
    console.error(e.message);
}
