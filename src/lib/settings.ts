// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import {config} from "../config/config";
import {Event} from "./events";
import {info, isTor} from "./helpers";

/**
 * Clear settings from sync storage
 */
export const clear = async () => {
    try {
        return await browser.storage.sync.remove("settings");
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Generate initial settings
 * @returns {Promise<{invidious; isEnabled; nitter; icons}>}
 */
const initSettings = async () => {
    try {
        const settings = config;

        settings.invidious.appSettings.instance = await isTor() ?
            config.invidious.appSettings.torInstance
            : config.invidious.appSettings.instance;

        return settings;
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Load settings from sync storage.
 *
 * @return Object
 */
export const load = async () => {
    try {
        const s = await browser.storage.sync.get("settings");

        if (s.settings) {
            const settings = s.settings;

            settings.invidious.appSettings.domains = config.invidious.appSettings.domains;
            settings.nitter.appSettings.domains = config.nitter.appSettings.domains;

            return {...config, ...settings};
        }

        return await initSettings();
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Save settings to sync storage.
 *
 * @param settings
 * @return Promise<T>
 */
export const save = async (settings: object) => {
    try {
        await browser.storage.sync.set({settings});

        info("Settings saved");
        browser.runtime.sendMessage(Event.SETTINGS_SAVE);
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Reset settings
 * @returns {Promise<void>}
 */
export const resetSettings = async () => {
    try {
        await clear();
        await save(await initSettings());
    } catch (e) {
        console.error(e.message);
    }
};
