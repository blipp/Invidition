import * as _ from "lodash";
// @ts-ignore
import {browser} from "webextension-polyfill-ts";
import * as InvidiousManager from "./lib/API/invidious";
import * as NitterManager from "./lib/API/nitter";
import * as BackwardCompatibilityManager from "./lib/backward_compatibility";
import {toggleIcon} from "./lib/browser_action";
import {Event} from "./lib/events";
import {canWriteToClipboard, domainsToRedirect, FILTER, filterDomains, filterTypes, info, isEnabled, SERVICE} from "./lib/helpers";
import {toggleCleanable} from "./lib/page_action";

/**
 * Handle CSP policy to allow embeds everywhere
 * Code inspired from https://github.com/ThomazPom/Moz-Ext-Ignore-X-Frame-Options/
 * @param r
 */
const injectCSP = async (r) => {
    try {
        info(`Injecting CSP for ${r.url}`);

        const headersToDelete = ["content-security-policy", "x-frame-options"];
        const sourceFilters = ["http://*", "http://*:*", "https://*", "https://*:*", "file://*"].join(" ");
        let csp = "";

        r.responseHeaders = r.responseHeaders.filter((h) => {
            const name = h.name.toLowerCase();

            csp = name === headersToDelete[0] ? h.value : csp;

            return !_.includes(headersToDelete, name);
        });

        csp = replaceCSP(csp, "frame-ancestors", sourceFilters);
        csp = replaceCSP(csp, "media-src", sourceFilters);
        csp = replaceCSP(csp, "frame-src", sourceFilters);

        r.responseHeaders.push({
                                   name: headersToDelete[0],
                                   value: csp,
                               });

        return {
            responseHeaders: r.responseHeaders,
        };
    } catch (e) {
        console.error(e.message);

        return {};
    }
};

const listenCSP = async (service?: SERVICE) => {
    if (browser.webRequest.onHeadersReceived.hasListener(injectCSP)) {
        browser.webRequest.onHeadersReceived.removeListener(injectCSP);
    }

    const urls = await filterDomains(FILTER.CSP, service);
    let types = null;

    switch (service) {
        case SERVICE.CSP:
            types = ["main_frame"];
            break;
        case SERVICE.INVIDIOUS:
            types = ["sub_frame", "object"];
            break;
        case SERVICE.NITTER:
            types = ["sub_frame", "object"];
            break;
    }

    const filter = types !== null ? {urls, types} : {urls};

    browser.webRequest.onHeadersReceived.addListener(
        injectCSP,
        filter,
        ["blocking", "responseHeaders"],
    );
};

/**
 * Handle Invidious redirections
 * @param r
 * @returns {Promise<{} | {redirectUrl: string} | {redirectUrl: string}>}
 */
const redirectInvidious = async (r) => {
    try {
        info(`Trying to redirect ${r.url}`);

        listenCSP(SERVICE.INVIDIOUS);

        const newUrl = await InvidiousManager.generateUrl(new URL(r.url));

        info(`Redirecting to ${newUrl.href}`);
        return {
            redirectUrl: newUrl.href,
        };
    } catch (e) {
        console.error(e.message);
    }
};

/**
 * Handle Nitter redirections
 * @param r
 * @returns {Promise<{} | {redirectUrl: string}>}
 */
const redirectNitter = async (r) => {
    info(`Trying to redirect ${r.url}`);

    listenCSP(SERVICE.NITTER);

    const newUrl = await NitterManager.generateUrl(new URL(r.url));

    info(`Redirecting to ${newUrl.href}`);

    return {
        redirectUrl: newUrl.href,
    };
};

/**
 * Handle redirections
 * @param r
 * @returns {Promise<{}>}
 */
const redirect = async (r) => {
    const domains = await domainsToRedirect();
    const url = new URL(r.url);

    if (domains.nitter.indexOf(url.hostname) !== -1) {
        if (await NitterManager.mustRedirect(r)) {
            return await redirectNitter(r);
        }
    } else if (domains.invidious.indexOf(url.hostname) !== -1) {
        if (await InvidiousManager.mustRedirect(r)) {
            return await redirectInvidious(r);
        }
    }

    return {};
};

const reload = async () => {
    if (browser.webRequest.onBeforeRequest.hasListener(await redirect)) {
        info("Reloading Invidition");
        browser.webRequest.onBeforeRequest.removeListener(await redirect);
    }

    if (!await isEnabled()) {
        return {};
    }

    const filter = {
        urls: await filterDomains(FILTER.REDIRECT),
        types: await filterTypes(FILTER.REDIRECT),
    };

    browser.webRequest.onBeforeRequest.addListener(
        redirect,
        filter,
        ["blocking"],
    );

    if (canWriteToClipboard() && await isEnabled("invidious")) {
        if (browser.tabs.onUpdated.hasListener(toggleCleanable)) {
            browser.tabs.onUpdated.removeListener(toggleCleanable);
        }

        browser.tabs.onUpdated.addListener(toggleCleanable);
    }
};

/**
 * replace a CSP value with another one
 * @param csp
 * @param {string} key
 * @param {[]} value
 * @returns {Promise<string>}
 */
const replaceCSP = (csp, key: string, value: string) => {
    return csp.includes(key) ? csp.replace(new RegExp(`${key}[^;]*;?`),
                                           `${key} ${value}; `,
    ) : `${key} ${value}; ${csp}`;
};

try {
    browser.runtime.onInstalled.addListener(
        BackwardCompatibilityManager.updateOldConfig,
    );
    browser.runtime.onInstalled.addListener(
        BackwardCompatibilityManager.welcomePage,
    );

    // Listen to messages received from the extension
    browser.runtime.onMessage.addListener(async (message) => {
        if (message === Event.SETTINGS_SAVE) { // On settings save
            await reload();
        }
    });

    toggleIcon().then();

    reload().then();
} catch (e) {
    console.error(e.message);
}
