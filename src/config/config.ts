export const config = {
    invidious: {
        appSettings: {
            alwaysUsePreferredInstance: false,
            api: {
                endpoint: "api/v1",
                instancesList: "https://instances.invidio.us/instances.json",
            },
            availableLanguages: [
                "ar",
                "de",
                "el",
                "en-US",
                "eo",
                "es",
                "eu",
                "fr",
                "is",
                "it",
                "nb_NO",
                "nl",
                "pl",
                "ru",
                "tr",
                "uk",
                "zh-CN",
                "zh-TW",
            ],
            domains: {
                toRedirect: [
                    "m.youtube.com",
                    "youtube.com",
                    "img.youtube.com",
                    "www.youtube.com",
                    "youtube-nocookie.com",
                    "www.youtube-nocookie.com",
                    "youtu.be",
                    "s.ytimg.com",
                ],
                toInterceptCSP: [
                    "www.qwant.com",
                    "www.youtube-nocookie.com",
                ],
                validPaths: [
                    "/channel",
                    "/embed",
                    "/login",
                    "/playlist",
                    "/preferences",
                    "/watch",
                ]
            },
            instance: "invidio.us",
            isEnabled: true,
            overrideCookies: true,
            random: false,
            rewriteLinks: false,
            torInstance: "axqzx4s6s54s32yentfqojs3x5i7faxza6xo3ehd4bzzsg2ii4fv2iid.onion",
            useParameters: true,
        },
        instanceSettings: {
            autoplay: false,
            continue: false,
            dark_mode: true,
            hl: "",
            listen: false,
            local: true,
            loop: false,
            nojs: 0,
            player_style: "youtube",
            quality: "dash",
            region: "",
            subtitles: ",,",
            thin_mode: false,
        },
    },
    isEnabled: true,
    nitter: {
        appSettings: {
            domains: {
                toRedirect: [
                    "twitter.com",
                    "www.twitter.com",
                    "mobile.twitter.com",
                    "pbs.twimg.com",
                    "video.twimg.com",
                ],
                toInterceptCSP: [],
            },
            instance: "nitter.net",
            isEnabled: true,
            random: false,
            rewriteLinks: false,
        },
    },
};
