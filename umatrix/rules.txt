# YouTube & YouTube IFRAME API
* img.youtube.com image allow
* youtube.com cookie block
* youtube.com image block
* youtube.com media block
* youtube.com script allow
* youtube.com frame allow
* youtube-nocookie.com cookie block
* youtube-nocookie.com image block
* youtube-nocookie.com media block
* youtube-nocookie.com script allow
* youtube-nocookie.com frame allow
* s.ytimg.com cookie block
* s.ytimg.com script allow

# Invidious - Replace the hostname with the instance you want to redirect to
* invidio.us frame allow
* invidious image allow
* invidio.us media allow
* invidio.us script allow
* invidio.us xhr allow
