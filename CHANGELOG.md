# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased


## [0.16.0]
### Changed
- Remove option to rewrite links. It needs a rewrite to reduce RAM and CPU usage.

## [0.15.2]
### Fixed
- Some optimizations to reduce CPU and memory usage.

## [0.15.1]
### Fixed
- [Invidious] Reduce CPU usage while watching a video.

## [0.15.0]
### Added
- [Invidious] [Nitter] Add an option to rewrite links on the webpage.

### Changed
- [Invidious] Update NoScript section in readme. Fix [#24](https://gitlab.com/Booteille/invidition/issues/24)

### Fixed
- Fix state of a service not being save each time, generating bugs when changing parameters via popup menu.
- Fix https://joinmastodon.org embed.
- Various cleanup and minor fixes.

## [0.14.0] - 2019-10-11

### Added
- [Invidious] Add `m.youtube.com` to domains to redirect.
- [Nitter] Enforce CSP for embeds. [#40](https://gitlab.com/Booteille/invidition/issues/40)

### Changed
- [Invidious] Always enable audio mode when toggled on, even if URL parameters are disabled. [#37](https://gitlab.com/Booteille/invidition/issues/37)

### Fixed
- [Nitter] Fix query missing from redirection. [#38](https://gitlab.com/Booteille/invidition/issues/38)

## [0.13.1] - 2019-10-09

### Added
- Updated italian translation, thanks to [@Unbranched](https://gitlab.com/unbranched) for having worked on this so fast!
- [Invidious] Add rule for uMatrix to allow `img.youtube.com` images. They are already redirected to Invidious so nothing changed in the code.
- [Invidious] Add `zh-TW` to available interface languages.

### Changed
- [Invidious] Now force the display of controls when controls are hidden and autoplay is on false.

### Fixed
- [Invidious] Fix pathname missing in cleaned URL.
- [Invidious] Remove unused youtube scripts.

## [0.13.0] - 2019-10-09

### Added
- Add option to load comments without javascript. Fix [#39](https://gitlab.com/Booteille/invidition/issues/39).
- Add option to enable interface thin mode.

### Changed
- Use `pageAction` to copy cleaned Invidious link.
    - This option is now only available for supported browsers. (Firefox 63+)
- Remove `clipboardWrite` permission.
- Enforce CSP for embeds only. Fix [#35](https://gitlab.com/Booteille/invidition/issues/35).
- Fix some typos.
- Disabling a service in the popup menu now disable its fieldset too.

### Fixed
- Captions where not working due to the use of the wrong url parameter.
    - `captions` url parameter renamed to `subtitles`
- Fix some css issues.

## [0.12.0] - 2019-10-08

### Added
- Add option to disable URL parameters
- Add hints to some parameters to help to understand how these parameters work.
- Add hint about non up to date instances.
- Add `region` option.
- Add "welcome" page when updating from anterior version.

### Changed
- `local`, `continue`, `autoplay` and `listen` parameters use numeric value, to match with instances using version anterior of [68be24ff](https://github.com/omarroth/invidious/commit/68be24ffc69bdd38735faaa87a32d885f47f0ec9).

- Fix issue where redirection was looping forever when `dash` quality was enabled and the target video was geo-blocked

## [0.11.1] - 2019-10-07

### Added
- Add `png` version of the logo.

### Fixed
- Remove forgotten `console.log`.
- Fix issue where backward compability was trying to keep extension state from the previous version. 

## [0.11.0] - 2019-10-07

### Added
- Integrate [Nitterify](https://gitlab.com/Booteille/nitterify): You can now redirect Twitter links to [Nitter](https://github.com/zedeus/nitter/)!
- You no longer need to search for an Invidious instance. All known instances are listed in settings. To achieve that, Invidition calls the [instances.invidio.us](https://instances.invidio.us) API.
- Add an option to select the fastest Invidious instance for you.
- Add the ability to select a custom Invidious instance instead of one of the provided list. (`localhost` supported. To use it with different port than 80, you'll need to use a reverse proxy) Fix [#33](https://gitlab.com/Booteille/invidition/issues/33).
- Add a button to choose a random Invidious instance.
- Add an option to overwrite instance cookies (enabled by default).
- Add an option to use audio mode (disabled by default). Fix [#29](https://gitlab.com/Booteille/invidition/issues/29).
- Add an option to always loop the video (disabled by default).
- Add an option to select video quality (default is `dash`). Fix [#5](https://gitlab.com/Booteille/invidition/issues/5).
- Add an option to select the player style (default is `youtube`). Fix [#22](https://gitlab.com/Booteille/invidition/issues/22).
- Add an option to enable dark mode (enabled by default).
- Add a popup menu with some useful shortcuts for everyday uses of Invidition. Fix [#15](https://gitlab.com/Booteille/invidition/issues/15).
- Add a link to copy URL without parameters in the popup. Fix [#27](https://gitlab.com/Booteille/invidition/issues/27), [#16](https://gitlab.com/Booteille/invidition/issues/16).
- Add backward compatibility with older versions. Your settings will be migrated once you installed the new version.
- Add `noreferrer` and `noopener` attributes to links on the settings page.
- Rewrite CSP policies to make Invidious work with more embeds.
- Better support of the Tor Browser. To achieve that and detect if the user is using Tor, a request to `http://expyuzz4wqqyqhjn.onion/` (URL of the Tor Project) is done.
- Better support of the Icecat Browser.

### Fixed
- No longer open instances list on the current tab, on Firefox for Android.
- Fix `Autoplay next video` option.
- More embeds now work. Fix [#26](https://gitlab.com/Booteille/invidition/issues/26).

### Changed
- New icon! Made by the awesome [Nicolas Llopis](https://www.llopisnicolas.com/) !
- Ask for new permissions: `clipboardWrite` (To be able to copy cleaned link to clipboard).
- Change the structure of `config.ts`.
- Settings are saved automatically on change.
- Update locale `www-widget.js` dependency
- Save the state of the extension, even if the browser is closed. Fix [#21](https://gitlab.com/Booteille/invidition/issues/21).
- Change a bit the design of settings page.

## [0.10.3] - 2019-09-27

### Added
- Italian translation, thanks to [@Unbranched](https://gitlab.com/unbranched)! (See [!1](https://gitlab.com/Booteille/invidition/merge_requests/1))
- Updated the list of available languages (Fix [#17](https://gitlab.com/Booteille/invidition/issues/17))

### Fixed
- Do not redirect subdomains from the Invidious instance.

## [0.10.2] - 2019-04-15

### Changed

- Add color to logos.

## [0.10.1] - 2019-04-15

### Fixed

- Does not add `autoplay_next` parameter in URL.

## [0.10.0] - 2019-04-15

### Added

- Add an option to autoplay the next videos when the box is checked on Invidious.

### Changed

- Interface language is now set up for all webpages of Invidious.

## [0.9.1] - 2019-04-11

### Fixed

- Fix issue where a non-supported interface language was reloading the page forever.
- Set english as default interface language now.

## [0.9.0] - 2019-04-11

### Added

- Add interface language selection.

## [0.8.1] - 2019-04-10

### Changed

- Don't use innerHTML anymore

## [0.8.0] - 2019-04-10

### Added

- Updated uMatrix recipe
- Internationalization support! Supported languages are english and french.

### Changed

- List subtitles by native name

## [0.7.3] - 2019-04-09

### Added

- Update uMatrix recipe to support known Invidious instances

### Fixed

- Don't force HTTPS for .onion domains
- Move 64x64 icons in the right directory.

## [0.7.2] - 2019-04-07

### Fixed

- Remove forgotten console.log()
- Fix issue with instance not being updated correctly.

## [0.7.1] - 2019-04-06

### Fixed

- No longer redirect instance subdomains to the main domain

## [0.7.0] - 2019-04-06

### Added

- Add "Force Proxy" option
- Add "Autoplay" option
- Add "Default Captions" option
- Apply selected options even when using direct Invidious links.

### Changed

- Rework with Typescript and use Webpack

## [0.6.0] - 2019-04-05

### Added

- Add 64x64 logos
- Add support for youtu.be

## [0.5.0] - 2019-04-01### Added

- Add README.md
- Add logo
- Add toolbar button to toggle addon activation
- Add uMatrix rules and recipes

## [0.4.0] - 2019-03-31

### Added

- Add CHANGELOG.md
- Now videos are proxied by using "local=true" parameter. No more Google requests.

## [0.3.0] - 2019-03-31

### Added

- Add support for youtube-nocookie.com

## [0.2.1] - 2019-03-31

### Fixed

- Add default value for instance setting
- Force usage of URL as instance setting to avoid issues

## [0.2.0] - 2019-03-31

### Added

- It is now possible to select which instance use

## [0.1.0] - 2019-03-31

### Added

- Initial release

[0.16.0]: https://codeberg.org/booteille/invidition/compare/v0.15.0...0.16.0
[0.15.0]: https://codeberg.org/booteille/invidition/compare/v0.14.0...0.15.0
[0.14.0]: https://codeberg.org/booteille/invidition/compare/v0.13.1...0.14.0
[0.13.1]: https://codeberg.org/booteille/invidition/compare/v0.13.0...0.13.1
[0.13.0]: https://codeberg.org/booteille/invidition/compare/v0.12.0...0.13.0
[0.12.0]: https://codeberg.org/booteille/invidition/compare/v0.11.1...0.12.0
[0.11.1]: https://codeberg.org/booteille/invidition/compare/v0.11.0...0.11.1
[0.11.0]: https://codeberg.org/booteille/invidition/compare/v0.10.3...0.11.0
[0.10.3]: https://codeberg.org/booteille/invidition/compare/v0.10.2...0.10.3
[0.10.2]: https://codeberg.org/booteille/invidition/compare/v0.10.1...0.10.2
[0.10.1]: https://codeberg.org/booteille/invidition/compare/v0.10.0...0.10.1
[0.10.0]: https://codeberg.org/booteille/invidition/compare/v0.9.1...0.10.0
[0.9.1]: https://codeberg.org/booteille/invidition/compare/v0.9.0...0.9.1
[0.9.0]: https://codeberg.org/booteille/invidition/compare/v0.8.1...0.9.0
[0.8.1]: https://codeberg.org/booteille/invidition/compare/v0.8.0...0.8.1
[0.8.0]: https://codeberg.org/booteille/invidition/compare/v0.7.3...0.8.0
[0.7.3]: https://codeberg.org/booteille/invidition/compare/v0.7.2...0.7.3
[0.7.2]: https://codeberg.org/booteille/invidition/compare/v0.7.1...0.7.2
[0.7.1]: https://codeberg.org/booteille/invidition/compare/v0.7.0...0.7.1
[0.7.0]: https://codeberg.org/booteille/invidition/compare/v0.6.0...0.7.0
[0.6.0]: https://codeberg.org/booteille/invidition/compare/v0.5.0...0.6.0
[0.5.0]: https://codeberg.org/booteille/invidition/compare/v0.4.0...0.5.0
[0.4.0]: https://codeberg.org/booteille/invidition/compare/v0.3.0...0.4.0
[0.3.0]: https://codeberg.org/booteille/invidition/compare/ecbbf514...v0.3.0
[0.2.1]: https://codeberg.org/booteille/invidition/compare/c72a71bc...ecbbf514
[0.2.0]: https://codeberg.org/booteille/invidition/compare/dfaaa962...c72a71bc
[0.1.0]: https://codeberg.org/booteille/invidition/compare/a479fcb3...dfaaa962
